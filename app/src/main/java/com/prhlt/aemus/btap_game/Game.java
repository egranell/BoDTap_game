package com.prhlt.aemus.btap_game;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Game extends Activity {

    private static final String TAG = "BTapGame";

    String root = "/BTap/";
    File rootDir;
    File file;
    PrintStream ps;

    double initialTime;
    public Handler messageHandler = new MessageHandler();
    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.i(TAG, "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "onServiceDisconnected");
        }
    };
    private GameView viewGame;

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        viewGame.exit();
                    }
                }).create().show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ball);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        viewGame = (GameView) findViewById(R.id.viewBall);
        viewGame.setLevel(getIntent().getExtras().getInt("level"));
        viewGame.setFather(this);
        initialTime = getIntent().getExtras().getDouble("initialTime");

        ComponentName componentName = new ComponentName("com.prhlt.aemus.BoDTapService",
                "com.prhlt.aemus.BoDTapService.BoDTapService");
        Intent intent = new Intent();
        intent.putExtra("MESSENGER", new Messenger(messageHandler));
        intent.setComponent(componentName);

        getApplication().bindService(intent, mServerConn, Context.BIND_AUTO_CREATE);
        ComponentName c = getApplication().startService(intent);

        if (c == null) {
            Toast.makeText(getApplicationContext(), "Failed to start the βTap Service", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Failed to start the βTap Service with " + intent);
            new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(4000);
                        finish();
                        System.exit(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        } else {
            Toast.makeText(getApplicationContext(), "βTap Service started", Toast.LENGTH_LONG).show();
            Log.i(TAG, "βTap Service started with " + intent);

            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

            if (settings.getBoolean("log", false)) {
                rootDir = new File(Environment.getExternalStorageDirectory().getPath() + root);
                if (!rootDir.exists()) {
                    if (!rootDir.mkdirs()) {
                        Log.e(TAG, "Cannot create directory: " + rootDir);
                    }
                }

                file = new File(rootDir.getAbsolutePath() + "/"+ TAG + "_"
                        + android.provider.Settings.Secure.getString(getApplicationContext().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID) + "_"
                        +  initialTime + ".csv");
                try {
                    FileOutputStream fos = new FileOutputStream(file, true);
                    ps = new PrintStream(fos);

                    if (!file.exists()) {
                        file.createNewFile();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "File write failed: " + e.toString());
                }

                ps.println(getResources().getString(R.string.logHeader));
                ps.flush();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewGame.getThread().toPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewGame.getThread().toResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ComponentName componentName = new ComponentName("com.prhlt.aemus.BoDTapService",
                "com.prhlt.aemus.BoDTapService.BoDTapService");
        Intent intent = new Intent();
        intent.setComponent(componentName);
        getApplication().stopService(intent);
        getApplication().unbindService(mServerConn);
    }

    public class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            JSONObject info = null;

            try {
                info = new JSONObject(message.getData().getString("data"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (settings.getBoolean("log", false)) {
                try {
                    info.put("requested",viewGame.possibleCollision());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ps.println(jsonToString(info));
                ps.flush();
            }

            int tap = 0;
            try {
                tap = info.getInt("tap");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            switch (tap) {
                case 0:
                    Log.d(TAG, "No βTAP!");
                    break;
                case 1:
                    Log.d(TAG, "βTAP_SINGLE!");
                    viewGame.onBTap_SINGLE_Event();
                    break;
                case 2:
                    Log.d(TAG, "βTAP_DOUBLE!");
                    viewGame.onBTap_DOUBLE_Event();
                    break;
                default:
                    Log.e(TAG, "βTAP Type not recognised!");
                    break;
            }

        }

        private String jsonToString(JSONObject data) {
            String info = "";

            try {
                info = data.get("time") + ",";
                info = info + data.get("tap") + ",";
                info = info + data.get("requested") + ",";
                info = info + data.get("microphone") + ",";

                JSONObject accJson = data.getJSONObject("accelerometer");
                info = info + accJson.get("X") + ",";
                info = info + accJson.get("Y") + ",";
                info = info + accJson.get("Z") + ",";

                JSONObject graJson = data.getJSONObject("gravity");
                info = info + graJson.get("X") + ",";
                info = info + graJson.get("Y") + ",";
                info = info + graJson.get("Z") + ",";

                JSONObject gyrJson = data.getJSONObject("gyroscope");
                info = info + gyrJson.get("X") + ",";
                info = info + gyrJson.get("Y") + ",";
                info = info + gyrJson.get("Z");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return info;
        }
    }
}