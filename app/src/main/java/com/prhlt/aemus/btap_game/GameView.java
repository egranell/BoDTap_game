package com.prhlt.aemus.btap_game;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

public class GameView extends View {
    private static final String TAG = "BTapGame";
    private static int PROCESS_PERIOD = 10;
    private float mX = 0, mY = 0;
    private boolean shot = false;
    private int squaresDestroyed = 0;
    private Activity father;
    private Graphic energy;
    private boolean energyActive = false;
    private int energyTime;
    private Context context;
    private gameThread thread = new gameThread();
    private long lastProcess = 0;
    private long startTime = 0;
    private Graphic circle;
    private Vector<Graphic> Squares;
    private int nSquares;
    private int nSquaresSingle = 0;
    private int nSquaresDouble = 0;
    private int level = 1;
    private int tapSINGLE = 0;
    private int tapDOUBLE = 0;
    private int tapSINGLEEffective = 0;
    private int tapDOUBLEEffective = 0;
   // private boolean doubleTap = false; // 0 Single Tap; 1 Double Tap

    private int doubleTapDelay = 200; //ms
    private long lastTap = 0;
    private Boolean squareSelected = false;

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        nSquares = Integer.valueOf(settings.getString("nsquares","30"));

        Drawable dcircle, dSquare, dEnergy;
        dSquare = context.getResources().getDrawable(R.drawable.square);
        Squares = new Vector<Graphic>();

        dcircle = context.getResources().getDrawable(R.drawable.circle);
        circle = new Graphic(this, dcircle);
        circle.setColor("#000000");
        circle.setAngle(0);
        circle.setHeigth(50);
        circle.setWidth(50);
        circle.setRadioColision(circle.getHeigth());

        dEnergy = context.getResources().getDrawable(R.drawable.circle);
        energy = new Graphic(this, dEnergy);
        energy.setHeigth(50);
        energy.setWidth(50);
        energy.setRadioColision(50);
        energy.setColor("#AAFF0000");

        for (int i = 0; i < nSquares; i++) {
            Graphic square = new Graphic(this, dSquare);

            square.setAngle((int) Math.random() % 360);
            square.setRotation((int) (Math.random() * 8 - 4));
            switch (level) {
                case 1:
                    square.setAccelerationY(Math.random() % 3 + 1);
                    square.setAccelerationX(0);
                    break;
                case 2:
                    square.setAccelerationY(Math.random() % 3 + 1);
                    square.setAccelerationX(0);
                    break;
                case 3:
                    //square.setAccelerationY(Math.random() % 3 + 1);
                    square.setAccelerationY(2);
                    square.setAccelerationX(0);
                    break;
                case 4:
                    square.setAccelerationY(Math.random() * 4 - 2);
                    square.setAccelerationX(Math.random() * 4 - 2);
                    break;
            }

            Squares.add(square);
        }
        startTime = System.currentTimeMillis();
    }

    public void exit() {
        Bundle bundle = new Bundle();
        bundle.putInt("level", level);
        bundle.putInt("squaresDestroyed", squaresDestroyed);
        bundle.putInt("nSquaresSingle", nSquaresSingle);
        bundle.putInt("nSquaresDouble", nSquaresDouble);
        bundle.putInt("tapSINGLE", tapSINGLE);
        bundle.putInt("tapDOUBLE", tapDOUBLE);
        bundle.putInt("tapSINGLEEffective", tapSINGLEEffective);
        bundle.putInt("tapDOUBLEEffective", tapDOUBLEEffective);
        bundle.putFloat("recallSINGLE", (float) tapSINGLEEffective / (nSquaresSingle==0?1:nSquaresSingle));
        bundle.putFloat("recallDOUBLE", (float) tapDOUBLEEffective / (nSquaresDouble==0?1:nSquaresDouble));
        bundle.putFloat("recall", (float) (tapSINGLEEffective + tapDOUBLEEffective) / nSquares);
        bundle.putFloat("precisionSINGLE", (float) (tapSINGLEEffective )/(tapSINGLE==0?1:tapSINGLE));
        bundle.putFloat("precisionDOUBLE", (float) (tapDOUBLEEffective)/(tapDOUBLE==0?1:tapDOUBLE));
        bundle.putFloat("precision", (float) (tapSINGLEEffective + tapDOUBLEEffective)/((tapSINGLE+tapDOUBLE)==0?1:tapSINGLE+tapDOUBLE));
        bundle.putString("time", String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - startTime),
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - startTime))
        ));
        Intent intent = new Intent();
        intent.putExtras(bundle);
        father.setResult(Activity.RESULT_OK, intent);
        father.finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (System.currentTimeMillis() - lastTap < doubleTapDelay) {
                    onBTap_DOUBLE_Event();
                } else {
                    onBTap_SINGLE_Event();
                }
                lastTap = System.currentTimeMillis();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        return true;
    }

    public void onBTap_SINGLE_Event() {
      //  doubleTap = false;
        Toast.makeText(getContext(),getResources().getText(R.string.tap),Toast.LENGTH_SHORT).show();
        tapSINGLE++;
        shot = true;
        ActivateEnergy(1);
    }

    public void onBTap_DOUBLE_Event() {
     //   doubleTap = true;
        Toast.makeText(getContext(),getResources().getText(R.string.tap2),Toast.LENGTH_SHORT).show();
        tapDOUBLE++;
        shot = true;
        ActivateEnergy(2);
    }

    public int possibleCollision(){
        try {
            for (Graphic square : Squares)
                if (circle.possibleCollision(square))
                    return square.getType();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return 0;
    }

    synchronized protected void updatePhysics() {
        long now = System.currentTimeMillis();
        if (lastProcess + PROCESS_PERIOD > now) {
            return;
        }

        double delay = (now - lastProcess) / PROCESS_PERIOD;
        lastProcess = now;

        for (int i=0;i< Squares.size();i++) {
            Graphic square = Squares.get(i);
            square.move(delay);

           // if (!squareSelected || square.getSelected()) {
                squareSelected = circle.possibleCollision(square);
            //}

            // Para hacer que cuando un cuadrado toque el círculo acabe la partida
            /*if (!(energyActive && square.verifyCollision(energy)) && square.verifyCollision(circle)) {
                exit();
            }*/

            if(square.getPositionY()>this.getHeight()){
                Squares.remove(i);
                i--;
                if (Squares.isEmpty()) {
                    exit();
                }
            }
        }

        energy.setPositionX(this.getWidth() / 2);
        energy.setPositionY(this.getHeight() / 2);

        if (energyActive) {
            energyTime -= delay;
            if (energyTime < 0) {
                energyActive = false;
            } else {
                for (int i = 0; i < Squares.size(); i++) {
                   // if (Squares.get(i).getSelected()  && energy.verifyCollision(Squares.get(i))) {
                    if (energy.verifyCollision(Squares.get(i))) {
                        destroysSquare(i);
                        break;
                    }
                }
            }

            if (energy.getHeigth() > 0) {
                energy.setHeigth(energy.getHeigth() - energy.getHeigth() / 25);
                energy.setWidth(energy.getWidth() - energy.getWidth() / 25);
             //   energy.setRadioColision(energy.getRadioCollision() - energy.getRadioCollision() / 25);
            }
        } else {
            if (energy.getHeigth() > 0) {
                energy.setHeigth(energy.getHeigth() - energy.getHeigth() / 50);
                energy.setWidth(energy.getWidth() - energy.getWidth() / 50);
         //       energy.setRadioColision(energy.getRadioCollision() - energy.getRadioCollision() / 50);
            }
        }
    }

    @Override
    protected void onSizeChanged(int width, int heigth, int oldWidth, int oldHeigth) {
        super.onSizeChanged(width, heigth, oldWidth, oldHeigth);
        for (int i = 0; i < Squares.size(); i++) {
            Graphic square = Squares.get(i);

            switch (level) {
                case 1:
//                    square.setPositionX(width / 2 + Math.pow(-1, i) * (circle.getWidth() / 2 + square.getWidth() / 2 + 15));
                    square.setPositionX(width / 2);
                    square.setType(1);
                    nSquaresSingle++;
                    //square.setPositionY(Math.random()%100*-1);
                    break;
                case 2:
                    square.setPositionX(width / 2 + Math.pow(-1, i) * (circle.getWidth() + square.getWidth()));
                    square.setType(2);
                    nSquaresDouble++;
                    //square.setPositionY(Math.random()%100*-1);
                    break;
                case 3:
                    if ((i % 2) == 0) {
                        square.setPositionX(width / 2);
                        square.setType(1);
                        //square.setPositionX(width / 2 + Math.pow(-1, i) * (circle.getWidth() / 2 + square.getWidth() / 2 + 15));
                        nSquaresSingle++;
                    }else {
                        square.setPositionX(width / 2 + Math.pow(-1, i) * (circle.getWidth() + square.getWidth()));
                        square.setType(2);
                        nSquaresDouble++;
                    }
                    break;
                case 4:
                    square.setPositionX(Math.random() % width);
                    square.setPositionY(Math.random() % heigth);
                    break;
            }

        }

        circle.setPositionX(width / 2);
        circle.setPositionY(heigth / 2);

        lastProcess = System.currentTimeMillis();
        thread.start();
    }

    @Override
    synchronized protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < Squares.size(); i++) {
            Graphic square = Squares.get(i);
            square.draw(canvas);
        }

        // if (energyActive)
        energy.draw(canvas);

        circle.draw(canvas);
        // Score Text
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(30);
       /* canvas.drawText(getResources().getText(R.string.time) +": "+ String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - startTime),
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - startTime))
                ),
                0, 40, paint);
        canvas.drawText(getResources().getText(R.string.precision)+"/"+getResources().getText(R.string.recall)+": " + String.format("%.2f",(float) squaresDestroyed /(nSquares==Squares.size()?1:nSquares-Squares.size())*100)+"%/"+
                String.format("%.2f",   (float) (tapSINGLEEffective + tapDOUBLEEffective)/((tapSINGLE+tapDOUBLE)==0?1:tapSINGLE+tapDOUBLE)*100)+"%",
                0, 160, paint);*/
        canvas.drawText(getResources().getText(R.string.goal)+": " + Squares.size(),
                0, 40, paint);
    }

    private void destroysSquare(int i) {
        squaresDestroyed++;
        if(Squares.get(i).getType() == 1)
            tapSINGLEEffective++;
        else
            tapDOUBLEEffective++;

        Squares.remove(i);
        if (Squares.isEmpty()) {
            exit();
        }
    }

    private void ActivateEnergy(int tapType) {
        switch (tapType){
            case 1:
                energy.setHeigth(150);
                energy.setWidth(150);
  //              energy.setRadioColision(150);
                break;
            case 2:
                energy.setHeigth(150);
                energy.setWidth(this.getWidth());
   //             energy.setRadioColision(this.getWidth());
                break;
        }
        energy.setPositionX(this.getWidth() / 2);
        energy.setPositionY(this.getHeight() / 2);
        energy.setAngle(0);
        energy.setAccelerationX(0);
        energy.setAccelerationY(0);
        energyTime = 30;
        energyActive = true;
    }

    public gameThread getThread() {
        return thread;
    }


    public void setFather(Activity father) {
        this.father = father;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;

        Drawable dSquare = context.getResources().getDrawable(R.drawable.square);
        Squares = new Vector<Graphic>();

        for (int i = 0; i < nSquares; i++) {
            Graphic square = new Graphic(this, dSquare);

            square.setAngle((int) Math.random() % 360);
            square.setRotation((int) (Math.random() * 8 - 4));
            square.setPositionY(-(i*750));
            switch (level) {
                case 1:
                    square.setAccelerationY(Math.random() % 3 + 1);
                    square.setAccelerationX(0);
                    break;
                case 2:
                    square.setAccelerationY(Math.random() % 3 + 1);
                    square.setAccelerationX(0);
                    break;
                case 3:
                   // square.setAccelerationY(Math.random() % 3 + 1);
                    square.setAccelerationY(2);
                    square.setAccelerationX(0);
                    break;
                case 4:
                    square.setAccelerationY(Math.random() * 4 - 2);
                    square.setAccelerationX(Math.random() * 4 - 2);
                    break;
            }


            Squares.add(square);
        }
    }

    public class gameThread extends Thread {
        private boolean pause, playing;

        public synchronized void toPause() {
            pause = true;
        }

        public synchronized void toResume() {
            pause = false;
            notify();
        }

        @Override
        public void run() {
            playing = true;
            while (playing) {
                updatePhysics();
                synchronized (this) {
                    while (pause) {
                        try {
                            wait();
                        } catch (Exception e) {
                        }
                    }
                    try {
                        Thread.sleep(PROCESS_PERIOD);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
