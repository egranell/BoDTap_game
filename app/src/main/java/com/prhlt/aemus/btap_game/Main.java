package com.prhlt.aemus.btap_game;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Main extends Activity {
    private static final String TAG = "BTapGame";

    private static final int GAME_ACTIVITY = 1;
    private Button bLevel1, bLevel2, bLevel3, bSend; //, bExit;
    //private TextView tScore1, tScore2, tScore3;
    double initialTime;
    String root = "/BTap/";
    File rootDir;
    File file;
    PrintStream ps;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 10;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        bLevel1 = (Button) findViewById(R.id.bLevel1);
        bLevel1.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                toPlay(1);
            }
        });

        bLevel2 = (Button) findViewById(R.id.bLevel2);
        bLevel2.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                toPlay(2);
            }
        });

        bLevel3 = (Button) findViewById(R.id.bLevel3);
        bLevel3.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                toPlay(3);
            }
        });


        bSend = (Button) findViewById(R.id.bSend);
        bSend.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                PackageInfo pInfo = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String version = pInfo.versionName;
                int verCode = pInfo.versionCode;

                Intent itSend = new Intent();
                //itSend.setType("*/*");
                itSend.setType("text/plain");
                //itSend.setType("message/rfc822");
                itSend.setAction(Intent.ACTION_SEND_MULTIPLE);

                itSend.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"speech4experiments@gmail.com"});
                itSend.putExtra(android.content.Intent.EXTRA_SUBJECT, "[βTap Game " + version + " (" + verCode + ")] Acquisition from " +
                        android.provider.Settings.Secure.getString(getApplicationContext().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID) +
                        " with " + android.os.Build.MODEL + " Android " + android.os.Build.VERSION.RELEASE);
               // itSend.putExtra(android.content.Intent.EXTRA_TEXT, "These are the records!");

                ArrayList<Uri> uris = new ArrayList<Uri>();
                //uris.add(Uri.parse("mailto:speech4experiments@gmail.com"));

                String root = "/BTap/";
                String zipFile = "/" + TAG + "_" +
                        android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID) +
                        ".zip";
                File rootDir = new File(Environment.getExternalStorageDirectory().getPath() + root);

                ArrayList<String> files = new ArrayList<String>();
                for (File f : rootDir.listFiles()) {
                    if (f.getName().startsWith(TAG) && (f.getName().endsWith(".csv") || f.getName().endsWith(".CSV"))) {
                        try {
                            files.add(f.getCanonicalPath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                try {
                    zip(files, rootDir + zipFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                uris.add(Uri.fromFile(new File(rootDir + zipFile)));
                itSend.putParcelableArrayListExtra(android.content.Intent.EXTRA_STREAM, uris);

                try {
                    startActivity(Intent.createChooser(itSend, "Send mail to βTap developers."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            rootDir = new File(Environment.getExternalStorageDirectory().getPath() + root);
            if (!rootDir.exists()) {
                if (!rootDir.mkdirs()) {
                    Log.e(TAG, "Cannot create directory: " + rootDir);
                }
            }
            file = new File(rootDir.getAbsolutePath() + "/" + TAG + "_"
                    + android.provider.Settings.Secure.getString(getApplicationContext().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID)
                    + ".csv");
            try {
                FileOutputStream fos = new FileOutputStream(file, true);
                ps = new PrintStream(fos);

                if (!file.exists()) {
                    file.createNewFile();
                }
            } catch (IOException e) {
                Log.e(TAG, "File write failed: " + e.toString());
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    public void toPlay(int level) {
        initialTime = System.currentTimeMillis();

        Intent i = new Intent(this, Game.class);
        Bundle bundle = new Bundle();
        bundle.putInt("level", level);
        bundle.putDouble("initialTime", initialTime);
        i.putExtras(bundle);

        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            if (file.length() == 0) {
                ps.println("Initial Time, User ID, Modality, Game Level, Playing Time, Squares Destroyed, Squares Not Destroyed, Single Distance Squares, Double Distance Squares, Single Taps Performed, Double Taps Performed, Squares Destroyed by Single Taps, Squares Destroyed by Double Taps, Single Taps Precision, Double Taps Precision, Global Precision, Single Taps Recall, Double Taps Recall, Global Recall");
                ps.flush();
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }

        startActivityForResult(i, GAME_ACTIVITY);
    }

    public void toInstructions() {
        Intent i = new Intent(this, Instructions.class);
        startActivity(i);
    }

    public void toSettings() {
        Intent i = new Intent(this, Settings.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.instructions:
                toInstructions();
                break;
            case R.id.settings:
                toSettings();
                break;
            case R.id.exit:
                finish();
                break;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case GAME_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    int level = data.getExtras().getInt("level");
                   /* switch (level) {
                        case 1:
                            tScore1.setText(getResources().getString(R.string.level1) + ": " + String.format("%.2f",data.getExtras().getFloat("precision")) + "%/" + String.format("%.2f",data.getExtras().getFloat("recall")) + "%");
                            break;
                        case 2:
                            tScore2.setText(getResources().getString(R.string.level2) + ": " + String.format("%.2f",data.getExtras().getFloat("precision")) + "%/" + String.format("%.2f",data.getExtras().getFloat("recall")) + "%");
                            break;
                        case 3:
                            tScore3.setText(getResources().getString(R.string.level3) + ": " + String.format("%.2f",data.getExtras().getFloat("precision")) + "%/" + String.format("%.2f",data.getExtras().getFloat("recall")) + "%");
                            break;
                    }*/
                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

                    if (!settings.getBoolean("test", false)) {
                        ps.println(initialTime + ", " + settings.getString("userID", "") + ", "
                                + (settings.getBoolean("movement", false) == false ? "Static" : "Movement") + ", " //Static - Movement
                                + (level == 1 ? "BTAP SINGLE" : level == 2 ? "BTAP DOUBLE" : "BTAP SINGLE & DOUBLE") + ", "
                                + data.getExtras().getString("time") + ", "
                                + data.getExtras().getInt("squaresDestroyed") + ", "
                                + (Integer.valueOf(settings.getString("nsquares", "10")) - Integer.valueOf(data.getExtras().getInt("squaresDestroyed"))) + ", "
                                + data.getExtras().getInt("nSquaresSingle") + ", "
                                + data.getExtras().getInt("nSquaresDouble") + ", "
                                + data.getExtras().getInt("tapSINGLE") + ", "
                                + data.getExtras().getInt("tapDOUBLE") + ", "
                                + data.getExtras().getInt("tapSINGLEEffective") + ", "
                                + data.getExtras().getInt("tapDOUBLEEffective") + ", "
                                + data.getExtras().getFloat("precisionSINGLE") + ", "
                                + data.getExtras().getFloat("precisionDOUBLE") + ", "
                                + data.getExtras().getFloat("precision") + ", "
                                + data.getExtras().getFloat("recallSINGLE") + ", "
                                + data.getExtras().getFloat("recallDOUBLE") + ", "
                                + data.getExtras().getFloat("recall"));
                        ps.flush();
                        //   ps.close();

                        Toast.makeText(this, "Log saved on file: " + file, Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {

                /*if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }*/
                return;
            }
        }
    }

    public static void zip(ArrayList<String> files, String zipFile) throws IOException {
        int BUFFER_SIZE = 256;
        BufferedInputStream origin = null;

        new File(zipFile).delete();

        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
        try {
            byte data[] = new byte[BUFFER_SIZE];

            for (int i = 0; i < files.size(); i++) {
                FileInputStream fi = new FileInputStream(files.get(i));
                origin = new BufferedInputStream(fi, BUFFER_SIZE);
                try {
                    ZipEntry entry = new ZipEntry(files.get(i).substring(files.get(i).lastIndexOf("/") + 1));
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
                        out.write(data, 0, count);
                    }
                } finally {
                    origin.close();
                }
            }
        } finally {
            out.close();
        }
    }
}