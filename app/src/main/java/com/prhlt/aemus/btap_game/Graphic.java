package com.prhlt.aemus.btap_game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

public class Graphic {

    public static final int MAX_SPEED = 20;
    private Drawable drawable;
    private double positionX, positionY, accelerationX, accelerationY;
    private int angle, rotation, width, heigth, radioCollision;
    private View view;

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    private Boolean selected = false;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    private int type;
    private int color = Color.parseColor("#FF000000"); //Color.parseColor("#FFFFFFFF");

    public Graphic(View view, Drawable drawable) {
        this.view = view;
        this.drawable = drawable;
        width = drawable.getIntrinsicWidth();
        heigth = drawable.getIntrinsicHeight();
        radioCollision = (heigth + width) / 4;
    }

    public void draw(Canvas canvas) {
        canvas.save();
        int x = (int) (positionX + width / 2);
        int y = (int) (positionY + heigth / 2);
        canvas.rotate((float) angle, (float) x, (float) y);
        drawable.setBounds((int) positionX, (int) positionY, (int) positionX + width, (int) positionY + heigth);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        drawable.draw(canvas);
        canvas.restore();
        int r = (int) Math.hypot(width, heigth) / 2 + MAX_SPEED;
        view.invalidate(x - r, y - r, x + r, y + r);
    }

    public void move(double factor) {
        positionX += accelerationX * factor;
        if (positionX < -width / 2) {
            positionX = view.getWidth() - width / 2;
            //positionY = Math.random() * (view.getHeight() - heigth);
        }
        if (positionX > view.getWidth() - width / 2) {
            positionX = -width / 2;
           // positionY = Math.random() * (view.getHeight() - heigth);
        }

        positionY += accelerationY * factor;

        // Comentar para permitir empezar fuera de la pantalla
        /*if (positionY < -heigth / 2) {
            positionY = view.getHeight() - heigth / 2;
            //positionX = Math.random() * (view.getWidth() - width);
        }*/
       //Comentar para que no vuelva a salir por arriba
        /*if (positionY > view.getHeight() - heigth / 2) {
            positionY = -heigth / 2;
            //positionX = Math.random() * (view.getWidth() - width);
        }*/
        angle += rotation * factor;
    }

    public double distance(Graphic g) {
        return Math.hypot((positionX + width / 2) - (g.positionX + g.getWidth() / 2), (positionY + heigth / 2) - (g.positionY + g.getHeigth() / 2));
    }

    public boolean verifyCollision(Graphic g) {
        return this.getDrawingRect().intersect(g.getDrawingRect());
    }

    public boolean possibleCollision(Graphic g) {
        Rect limits = new Rect(0, ((int) this.getPositionY() - this.getHeigth()),
                view.getWidth(), ((int) this.getPositionY() + this.getHeigth()));

        if (Rect.intersects(limits, g.getDrawingRect())) {
            g.setColor("#FF00F00F");
            g.setSelected(true);
        } else {
            g.setColor("#FF000000");
            g.setSelected(false);
        }
        return g.getSelected();
    }

    public void setColor(String color) {
        this.color = Color.parseColor(color);
        this.drawable.setColorFilter(this.color, PorterDuff.Mode.SRC_ATOP);
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public double getPositionX() {
        return positionX;
    }

    public void setPositionX(double positionX) {
        this.positionX = positionX - this.width / 2;
    }

    public double getPositionY() {
        return positionY;
    }

    public void setPositionY(double positionY) {
        this.positionY = positionY - this.heigth / 2;
    }

    public double getAccelerationX() {
        return accelerationX;
    }

    public void setAccelerationX(double accelerationX) {
        this.accelerationX = accelerationX;
    }

    public double getAccelerationY() {
        return accelerationY;
    }

    public void setAccelerationY(double accelerationY) {
        this.accelerationY = accelerationY;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeigth() {
        return heigth;
    }

    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

    public int getRadioCollision() {
        return radioCollision;
    }

    public void setRadioColision(int radioColision) {
        this.radioCollision = radioColision;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public Rect getDrawingRect() {
        return new Rect((int) this.getPositionX(), (int) this.getPositionY(), (int) this.getPositionX() + this.getWidth(), (int) this.getPositionY() + this.getHeigth());
    }

}
